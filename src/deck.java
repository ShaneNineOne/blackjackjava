import java.util.Arrays;
import java.util.Collections;

public class deck {
	public byte currentCard;
	card[] cards = new card[52];
	
	public void loadCards() {
		System.out.println("Loading cards...");
		cards[0] = new card(1, "/ace_of_spades2.jpg");
		cards[1] = new card(1, "/ace_of_diamonds.jpg");
		cards[2] = new card(1, "/ace_of_hearts.jpg");
		cards[3] = new card(1, "/ace_of_clubs.jpg");
		cards[4] = new card(2, "/2_of_spades.jpg");
		cards[5] = new card(2, "/2_of_diamonds.jpg");
		cards[6] = new card(2, "/2_of_hearts.jpg");
		cards[7] = new card(2, "/2_of_clubs.jpg");
		cards[8] = new card(3, "/3_of_spades.jpg");
		cards[9] = new card(3, "/3_of_diamonds.jpg");
		cards[10] = new card(3, "/3_of_hearts.jpg");
		cards[11] = new card(3, "/3_of_clubs.jpg");
		cards[12] = new card(4, "/4_of_spades.jpg");
		cards[13] = new card(4, "/4_of_diamonds.jpg");
		cards[14] = new card(4, "/4_of_hearts.jpg");
		cards[15] = new card(4, "/4_of_clubs.jpg");
		cards[16] = new card(5, "/5_of_spades.jpg");
		cards[17] = new card(5, "/5_of_diamonds.jpg");
		cards[18] = new card(5, "/5_of_hearts.jpg");
		cards[19] = new card(5, "/5_of_clubs.jpg");
		cards[20] = new card(6, "/6_of_spades.jpg");
		cards[21] = new card(6, "/6_of_diamonds.jpg");
		cards[22] = new card(6, "/6_of_hearts.jpg");
		cards[23] = new card(6, "/6_of_clubs.jpg");
		cards[24] = new card(7, "/7_of_spades.jpg");
		cards[25] = new card(7, "/7_of_diamonds.jpg");
		cards[26] = new card(7, "/7_of_hearts.jpg");
		cards[27] = new card(7, "/7_of_clubs.jpg");
		cards[28] = new card(8, "/8_of_spades.jpg");
		cards[29] = new card(8, "/8_of_diamonds.jpg");
		cards[30] = new card(8, "/8_of_hearts.jpg");
		cards[31] = new card(8, "/8_of_clubs.jpg");
		cards[32] = new card(9, "/9_of_spades.jpg");
		cards[33] = new card(9, "/9_of_diamonds.jpg");
		cards[34] = new card(9, "/9_of_hearts.jpg");
		cards[35] = new card(9, "/9_of_clubs.jpg");
		cards[36] = new card(10, "/10_of_spades.jpg");
		cards[37] = new card(10, "/10_of_diamonds.jpg");
		cards[38] = new card(10, "/10_of_hearts.jpg");
		cards[39] = new card(10, "/10_of_clubs.jpg");
		cards[40] = new card(10, "/jack_of_spades2.jpg");
		cards[41] = new card(10, "/jack_of_diamonds2.jpg");
		cards[42] = new card(10, "/jack_of_hearts2.jpg");
		cards[43] = new card(10, "/jack_of_clubs2.jpg");	
		cards[44] = new card(10, "/queen_of_spades2.jpg");
		cards[45] = new card(10, "/queen_of_diamonds2.jpg");
		cards[46] = new card(10, "/queen_of_hearts2.jpg");
		cards[47] = new card(10, "/queen_of_clubs2.jpg");
		cards[48] = new card(10, "/king_of_spades2.jpg");
		cards[49] = new card(10, "/king_of_diamonds2.jpg");
		cards[50] = new card(10, "/king_of_hearts2.jpg");
		cards[51] = new card(10, "/king_of_clubs2.jpg");
	}
	
	public void shuffleDeck() {
		Collections.shuffle(Arrays.asList(cards));
		currentCard = 0;
	}
	
	public card drawCard() {
		currentCard++;
		return cards[currentCard - 1];
	}
}
