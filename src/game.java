import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;

public class game extends Application {
	public enum Gamestate {PLAYERTURN, OPPONENTTURN, RESULTS};
	Gamestate state;
	deck cards = new deck();
	int playerTotal = 0;
	int opponentTotal = 0;
	Timeline timeline;
    Label playerStats;
    Label opponentStats;
    Label gameStateLabel;
    
    Button hitButton;
    Button stayButton;
    Button newGameButton;
    
    HBox controls;
    
    HBox opponentCards;
    HBox playerCards;
	
    public void start(Stage primaryStage) {   
    	cards.loadCards();
    	playerStats = new Label();
    	opponentStats = new Label();
    	gameStateLabel = new Label();
    	
    	hitButton = new Button();
		stayButton = new Button();
		newGameButton = new Button();
		
		controls = new HBox();
		opponentCards = new HBox();
		playerCards = new HBox();
		
		GridPane mainPane = new GridPane();        
	    // Position mainPane for center       
	    mainPane.setAlignment(Pos.CENTER_LEFT);       
	    // Set spaces between controls            
	    mainPane.setHgap(20); 
	    mainPane.setVgap(40);   
	    
	    mainPane.setStyle("-fx-background-image: url('/tableBackground.jpg'); " +
	            "-fx-background-position: center center; " +
	            "-fx-background-repeat: stretch;");
	    
	    Font gsFont = Font.font("Arial", FontWeight.BOLD, 22);
	    Font GUIFont = Font.font("Arial", FontWeight.BOLD, 18);
	    gameStateLabel.setText("Your turn");
	    gameStateLabel.setTextFill(Color.DODGERBLUE);
	    gameStateLabel.setFont(gsFont);
	    gameStateLabel.setMaxWidth(200);
	    gameStateLabel.setWrapText(true);
	    
	    playerStats.setText("Your total: 1");
	    opponentStats.setText("Opponent's total: 3");
	    playerStats.setTextFill(Color.DODGERBLUE);
	    opponentStats.setTextFill(Color.RED);
	    playerStats.setFont(GUIFont);
	    opponentStats.setFont(GUIFont);
	 
	    controls.setSpacing(5);
	    controls.getChildren().add(hitButton);
	    controls.getChildren().add(stayButton);
	    
	    hitButton.setText("Hit");
	    stayButton.setText("Stay");
	    newGameButton.setText("New Game");
	    
	    
	    //(top/right/bottom/left)
	    playerStats.setPadding(new Insets(0, 0, 0, 15));
	    opponentStats.setPadding(new Insets(0, 0, 0, 15));
	    controls.setPadding(new Insets(0, 0, 0, 15));
	    gameStateLabel.setPadding(new Insets(0, 0, 0, 15));
	    
	    opponentCards.setSpacing(-75);
	    playerCards.setSpacing(-75);  
	    
	    initialSetup();
	    
	    mainPane.add(opponentCards, 3, 0);
	    mainPane.add(gameStateLabel, 0, 2);
	    mainPane.add(playerCards, 3, 1);
	    mainPane.add(playerStats, 0, 1);
	    mainPane.add(opponentStats, 0, 0);
	    mainPane.add(controls, 0, 3);
	    
	    hitButton.setOnAction(e -> {
	    	if (state == Gamestate.PLAYERTURN) {
		    	card drawn = cards.drawCard();
		    	playerTotal += drawn.value;
		    	playerStats.setText("Your total: " + playerTotal);
		    	playerCards.getChildren().add(new ImageView(drawn.imagePath));
		    	checkResults();	    		
	    	}
	    });
	    
	    stayButton.setOnAction(e -> {
	    	if (state == Gamestate.PLAYERTURN) {
		    	state = Gamestate.OPPONENTTURN;
		    	checkResults();
		    	if (state != Gamestate.RESULTS) {
					gameStateLabel.setTextFill(Color.RED);
					gameStateLabel.setText("OPPONENT'S TURN");
					timeline = new Timeline(new KeyFrame(
						Duration.millis(1000), 
						ae -> opponentRound()));
					timeline.setCycleCount(Animation.INDEFINITE);
					timeline.play();
		    	}
	    	}
	    });
	    
	    newGameButton.setOnAction(e -> {
	    	controls.getChildren().clear();
		    controls.getChildren().add(hitButton);
		    controls.getChildren().add(stayButton);
		    initialSetup();
	    });
	    
        Scene blackjackScene = new Scene(mainPane, 1000, 600);  
        
        primaryStage.setTitle("Blackjack");        
        primaryStage.setScene(blackjackScene); 
   		primaryStage.show();    
    }
    
    public void launchWith(String[] args) {
    	launch(args);
    }
    
    public void initialSetup() {
    	opponentCards.getChildren().clear();
    	playerCards.getChildren().clear();
    	
    	playerTotal = 0;
    	opponentTotal = 0;
    	cards.shuffleDeck();
    	
    	card c1 = cards.drawCard();
    	card c2 = cards.drawCard();
    	card c3 = cards.drawCard();
    	card c4 = cards.drawCard();
    	
    	playerTotal = c1.value + c2.value;
    	playerCards.getChildren().add(new ImageView(c1.imagePath));
    	playerCards.getChildren().add(new ImageView(c2.imagePath));
    	
    	opponentTotal = c3.value + c4.value;
    	opponentCards.getChildren().add(new ImageView(c3.imagePath));
    	opponentCards.getChildren().add(new ImageView(c4.imagePath));
    	
    	playerStats.setText("Your total: " + playerTotal);
    	opponentStats.setText("Opponent's Total: " + opponentTotal);
    	
		gameStateLabel.setTextFill(Color.CORNFLOWERBLUE);
		gameStateLabel.setText("YOUR TURN");
		
    	state = Gamestate.PLAYERTURN;
    	checkResults();
    }
    
    public void opponentRound() {
		if (state == Gamestate.OPPONENTTURN) {
				
			card nextCard = cards.drawCard();
    		opponentTotal += nextCard.value;
    		opponentCards.getChildren().add(new ImageView(nextCard.imagePath));
    		opponentStats.setText("Opponent's Total: " + opponentTotal);
    		checkResults();    	
		}	
		
		if (state == Gamestate.RESULTS) {
			timeline.stop();
		}
    }
    
    public void checkResults() {
    	if (state == Gamestate.PLAYERTURN) {
    		if (playerTotal > 21) {
    			state = Gamestate.RESULTS;
    			controls.getChildren().clear();
    			controls.getChildren().add(newGameButton);
    			gameStateLabel.setTextFill(Color.RED);
    			gameStateLabel.setText("BUST!!! Your opponent has won this round!");
    		} else if (playerTotal == 21) {
    			state = Gamestate.RESULTS;    	
    			controls.getChildren().clear();
    			controls.getChildren().add(newGameButton);
    			gameStateLabel.setTextFill(Color.CORNFLOWERBLUE);
    			gameStateLabel.setText("BLACKJACK!!! You have won this round!");
    		}
    	} else if (state == Gamestate.OPPONENTTURN) {
    		if (opponentTotal > 21) {
    			state = Gamestate.RESULTS;
    			controls.getChildren().clear();
    			controls.getChildren().add(newGameButton);
    			gameStateLabel.setTextFill(Color.CORNFLOWERBLUE);
    			gameStateLabel.setText("Your opponent has gone BUST!!! You have won this round!");
    		} else if (opponentTotal == 21) {
    			state = Gamestate.RESULTS;    	
    			controls.getChildren().clear();
    			controls.getChildren().add(newGameButton);
    			gameStateLabel.setTextFill(Color.RED);
    			gameStateLabel.setText("BLACKJACK!!! Your opponent has won this round!!");
    		} else if (opponentTotal > playerTotal) {
    			state = Gamestate.RESULTS;  
    			controls.getChildren().clear();
    			controls.getChildren().add(newGameButton);
    			gameStateLabel.setTextFill(Color.RED);
    			gameStateLabel.setText("Your opponent has outscored you and won this round!");    			
    		}
    	}
    };
}
